  function cleanTextareaEdge(){
    document.getElementById("textareaEdge").value="";
  }
  
  // Gets the hidden input for new edges
  // results:
  // source_node_id - target_node_id if there is new edge
  // -1 case there is no new edge
  function getNewEdge(){
    return document.getElementById("new_edge").value;
  }
  
  // Returns the object node with id = id (parameter)
  function getNode(thisGraph,id){
    return thisGraph.nodes.filter(function(n){return n.id == id;})[0];
  }
  
  // Returns the id of the source node
  // Is the value of the hidden input for source node
  function getSourceNodeId(){
   return document.getElementById("source_node_id").value;
 }

  // Returns the id of the target node
  // Is the value of the hidden input for target node
  function getTargetNodeId(){
   return document.getElementById("target_node_id").value;
 }

  // Set the hidden input for new edges with the value of the parameter "edge"
  // "edge" should be -1 if no new edge or source_node_id-target_node_id if there is a new edge
  function setNewEdge(edge){
   document.getElementById("new_edge").value = edge;
 }

  // Set the hidden input for Source node with "id"
  function setSourceNode(id){
   document.getElementById("source_node_id").value = id;
 }

  // Set the hidden input for Target node with "id"
  function setTargetNode(id){
   document.getElementById("target_node_id").value = id;
 }

 function clearRelatedTopicList(){
   d3.select("#related_list_ul").remove();
 }


// Functions to switch between tabs
  function showIndexSide(){
   d3.select("#index-side").style("display","block");
   d3.select("#sentences-side").style("display","none");
   d3.select("#help-side").style("display","none");
   d3.select("#download-side").style("display","none");
 }

 function showHelpSide(){
   d3.select("#help-side").style("display","block");
   d3.select("#index-side").style("display","none");
   d3.select("#sentences-side").style("display","none");
   d3.select("#download-side").style("display","none");
 }

 function showSentencesSide(){
   d3.select("#sentences-side").style("display","block");
   d3.select("#index-side").style("display","none");
   d3.select("#help-side").style("display","none");
   d3.select("#download-side").style("display","none");
 }

 function showDownloadSide(){
   d3.select("#download-side").style("display","block");
   d3.select("#sentences-side").style("display","none");
   d3.select("#index-side").style("display","none");
   d3.select("#help-side").style("display","none");
 }

  // Hide the relationships textarea and text
  function hideTextareaRelationships(){
   //d3.select("#promp-relationships").style("display","none");
   $("#dialog-verb").dialog("close");
 }

  // Show the relationships textarea and text
  function showTextareaRelationships(){
   //d3.select("#promp-relationships").style("display","block");
     type = 0;
  switch (type){
    case 0 : 
        $("#dialog-verb").dialog("open");
    break;
  }

 }

 function showNodeAttributesDialog(){
    $("#dialog-node-attributes").dialog("open");

 }

 
// Set up the slider
$(function() {
  $('#slider').slider({
    orientation: 'vertical',
    min: 1000,
    max: 2000,
    value: 1000,
    slide: function( event, ui ) {
      zoomWithSlider(ui.value / 1000);
    }
  });
});

function zoomWithSlider(scale) {
  var svg = d3.select('body').select('svg');
  var container = d3.select('.container_slider');
  var h = svg.attr('height'), w = svg.attr('width');
  container.attr('transform','scale(' + scale + ') ');
}

$("#menu-toggle-sidebar").click(function(e) {
        e.preventDefault();
        $("#wrapper-sidebar").toggleClass("active");
});

$(function() {
          $("#dialog-verb").dialog({
                autoOpen: false,
                modal: true,
                minHeight: 265                
              });
      
          // Set the autocomplete tool for the verbs textarea
          $('#textareaEdge').autocomplete({
            source: ['causes', 'implies', 'increases', 'decreases', 'is',  'is not'],
            minLength: 0
          }).on('focus', function() {
            $(this).autocomplete('search', '');
          });

          $("#dialog-node-attributes").dialog({
                autoOpen: false,
                modal: true,
                minHeight: 265                
              });

 $(function() {
    $( "#tabs" ).tabs();
  });



          });

function getNodeAttributeInterface(){
    return $("input:radio[name=node-attribute]:checked").val();
  }



