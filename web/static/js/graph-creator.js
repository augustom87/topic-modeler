document.onload = (function(d3, saveAs, Blob, undefined){
  "use strict";

  // TODO add user settings
  var consts = {
    defaultTitle: "new"
  };
  var settings = {
    appendElSpec: "#graph"
  };
  // define graphcreator object
  var GraphCreator = function(svg, nodes, edges){
    var thisGraph = this;
    thisGraph.idct = 0;

    thisGraph.nodes = nodes || [];
    thisGraph.edges = edges || [];
    
    thisGraph.state = {
      selectedNode: null,
      selectedEdge: null,
      mouseDownNode: null,
      mouseDownLink: null,
      justDragged: false,
      justScaleTransGraph: false,
      lastKeyDown: -1,
      shiftNodeDrag: false,
      selectedText: null
    };

    // define arrow markers for graph links
    var defs = svg.append('svg:defs');
    defs.append('svg:marker')
    .attr('id', 'end-arrow')
    .attr('viewBox', '0 -5 10 10')
    .attr('refX', "32")
    .attr('markerWidth', 3.5)
    .attr('markerHeight', 3.5)
    .attr('orient', 'auto')
    .append('svg:path')
    .attr('d', 'M0,-5L10,0L0,5');

    // define arrow markers for leading arrow
    defs.append('svg:marker')
    .attr('id', 'mark-end-arrow')
    .attr('viewBox', '0 -5 10 10')
    .attr('refX', 7)
    .attr('markerWidth', 3.5)
    .attr('markerHeight', 3.5)
    .attr('orient', 'auto')
    .append('svg:path')
    .attr('d', 'M0,-5L10,0L0,5');

    thisGraph.svg = svg;
    var container = svg.append("g").classed("container_slider",true);
    thisGraph.svgG = container.append("g")
    .classed(thisGraph.consts.graphClass, true);
    var svgG = thisGraph.svgG;

    // displayed when dragging between nodes
    thisGraph.dragLine = svgG.append('svg:path')
    .attr('class', 'link dragline hidden')
    .attr('d', 'M0,0L0,0')
    .style('marker-end', 'url(#mark-end-arrow)');

    // svg nodes and edges
    thisGraph.paths = svgG.append("g").selectAll("g");
    thisGraph.circles = svgG.append("g").selectAll("g");
    thisGraph.texts = svgG.append("g").selectAll("g");

    thisGraph.drag = d3.behavior.drag()
    .origin(function(d){
      return {x: d.x, y: d.y};
    })
    .on("drag", function(args){
      thisGraph.state.justDragged = true;
      thisGraph.dragmove.call(thisGraph, args);
    })
    .on("dragend", function() {
            // todo check if edge-mode is selected
          });


    d3.select("#textareaSentencesInput").on("keydown", function(){	
      thisGraph.textareaSentencesInputDown.call(thisGraph);
    })	

    d3.select("#textareaTopic").on("keydown", function(){	
      thisGraph.textareaTopicKeyDown.call(thisGraph);
    })	

    d3.select("#textareaEdge").on("keydown", function(){	
      thisGraph.textareaEdgeKeyDown.call(thisGraph);
    })	

    // listen for key events
    d3.select(window).on("keydown", function(){
      thisGraph.svgKeyDown.call(thisGraph);
    })
    .on("keyup", function(){
      thisGraph.svgKeyUp.call(thisGraph);
    });
    
    svg.on("mousedown", function(d){thisGraph.svgMouseDown.call(thisGraph, d);});
    svg.on("mouseup", function(d){thisGraph.svgMouseUp.call(thisGraph, d);});
    svg.on("dblclick", function(d){thisGraph.svgDblclick.call(thisGraph, d3.select(this), d);});

    // listen for dragging
    var dragSvg = d3.behavior.zoom().scaleExtent([0.5, 2])
    .on("zoom", function(){
      if (d3.event.sourceEvent.shiftKey){
              // TODO  the internal d3 state is still changing
              return false;
            } else{
              thisGraph.zoomed.call(thisGraph);
            }
            return true;
          })
    .on("zoomstart", function(){
      var ael = d3.select("#" + thisGraph.consts.activeEditId).node();
      if (ael){
        ael.blur();
      }
      if (!d3.event.sourceEvent.shiftKey) d3.select('body').style("cursor", "move");
    })
    .on("zoomend", function(){
      d3.select('body').style("cursor", "auto");
    });

    svg.call(dragSvg).on("dblclick.zoom", null);	
    // listen for resize
    window.onresize = function(){thisGraph.updateWindow(svg);};

    d3.select("#list-button").on("click", function(){
      showIndexSide();
    });
    d3.select("#sentences-button").on("click", function(){
      showSentencesSide();
    });

    d3.select("#help-button").on("click", function(){
      showHelpSide();
    });

    d3.select("#download-button").on("click", function(){
      showDownloadSide();
    });
    

    // handle download data
    d3.select("#download-input").on("click", function(){
      var saveEdges = [];
      thisGraph.edges.forEach(function(val, i){
        saveEdges.push({source: val.source.id, target: val.target.id, title :val.title});
      });
      var blob = new Blob([window.JSON.stringify({"nodes": thisGraph.nodes, "edges": saveEdges})], {type: "text/plain;charset=utf-8"});
      saveAs(blob, "mydag.json");
    });

// -------------------------------- Listeners --------------------------------
	// Exports the graph in png
  // To export the graph, we move every node and path to the positive plane
  // Then we "cut" the graph only where nodes are visible and we export that area
d3.select("#download-jpg").on("click", function(){
  
  var image = new Image;
  // Looks for the min and max x and y  
  if (thisGraph.nodes.length>0){
      var x_min = thisGraph.nodes[0].x;
      var y_min = thisGraph.nodes[0].y;
      var x_max = thisGraph.nodes[0].x;
      var y_max = thisGraph.nodes[0].y;
  }
  for (var i = 1; i<thisGraph.nodes.length;i++){
    if (thisGraph.nodes[i].x < x_min){x_min=thisGraph.nodes[i].x }
    if (thisGraph.nodes[i].y < y_min){y_min=thisGraph.nodes[i].y }

    if (thisGraph.nodes[i].x > x_max){x_max=thisGraph.nodes[i].x }
    if (thisGraph.nodes[i].y > y_max){y_max=thisGraph.nodes[i].y }
  }
  
  //  Get transform attributes from the graph and the slider
  var t_graph = d3.transform(d3.select(".graph").attr("transform"));
  var tx_graph = t_graph.translate[0];
  var ty_graph = t_graph.translate[1];
  var tscale_graph = t_graph.scale[0];
  d3.transform(d3.select(".graph").attr("transform","translate(0,0)"));

  var t_container_slider = d3.transform(d3.select(".container_slider").attr("transform"));
  var tscale_container_slider = t_container_slider.scale[0];
  d3.transform(d3.select(".container_slider").attr("transform","translate(0,0)"));

  // Move every node
  for (var i = 0; i<thisGraph.nodes.length;i++){
    thisGraph.nodes[i].x-=(x_min)-200;
    thisGraph.nodes[i].y-=(y_min)-200;
  }
  
  // Update graph (it's so fast that we dont need to hide it to the user)
  thisGraph.updateGraph();

  var svg_height = d3.select("svg").attr("height");
  var svg_width = d3.select("svg").attr("width");
  
  d3.select("svg").attr("height",y_max +(y_min));
  d3.select("svg").attr("width",x_max +(x_min));
  
  var x =svgAsDataUri(document.getElementById("svg"), 1, function(uri) {  
      d3.select(".canvas").attr("height",y_max  + (y_min));
      d3.select(".canvas").attr("width",x_max  + (x_min));
  
      var canvas = document.querySelector("canvas"),    
      context = canvas.getContext("2d");
      
      image.src = uri;
        
        image.onload = function() {
           context.drawImage(image, 0, 0);

           var canvasdata = canvas.toDataURL("image/png");

           var pngimg = '<img src="'+canvasdata+'">'; 

           d3.select("#pngdataurl").html(pngimg);

           var a = document.createElement("a");
           a.download = "sample.png";
           a.href = canvasdata;
           a.click();
        }
       context.clearRect ( 0 , 0 , 4500 , 4500 );

    });
  
  //Return graph to original transformations and nodes to original position
  d3.select("svg").attr("height",svg_height);
  d3.select("svg").attr("width",svg_width);
  d3.transform(d3.select(".graph").attr("transform","translate(" + tx_graph + "," + ty_graph + ") scale(" + tscale_graph + ")"));
  d3.transform(d3.select(".container_slider").attr("transform","scale(" + tscale_container_slider + ")"));
  for (var i = 0; i<thisGraph.nodes.length;i++){
        thisGraph.nodes[i].x+=(x_min)-200;
        thisGraph.nodes[i].y+=(y_min)-200;
      } 
  thisGraph.updateGraph();
  });
	
  // Generate Sentences
  d3.select("#generate-sentences").on("click", function(){
    var lineBreak = "%0D%0A";
    var str = "";
    thisGraph.edges.forEach(function(val, i){
      var ids = val.id.split("-");
      var sou=getNode(thisGraph,ids[0]);
      var tar=getNode(thisGraph,ids[1]);
      var Source = sou.title[0].toUpperCase() + sou.title.substring(1);
      str += Source + " " + val.title.toLowerCase() + " " + tar.title.toLowerCase() + "." + lineBreak;
    });	

    var uri = 'data:text/csv;charset=utf-8,' + str;
    var downloadLink = document.createElement("a");
    downloadLink.href = uri;
    downloadLink.download = "data.txt";
    document.body.appendChild(downloadLink);
    downloadLink.click();
    document.body.removeChild(downloadLink);
  });

    // handle uploaded data
  d3.select("#upload-input").on("click", function(){
      document.getElementById("hidden-file-upload").click();
  });
  
  d3.select("#hidden-file-upload").on("change", function(){
      if (window.File && window.FileReader && window.FileList && window.Blob) {
        var uploadFile = this.files[0];
        var filereader = new window.FileReader();

        filereader.onload = function(){
          var txtRes = filereader.result;
          // TODO better error handling
          try{
            var jsonObj = JSON.parse(txtRes);
            thisGraph.deleteGraph(true);
            thisGraph.nodes = jsonObj.nodes;
            thisGraph.setIdCt(jsonObj.nodes.length + 1);
            var newEdges = jsonObj.edges;
            var defs = d3.select('svg defs');
            newEdges.forEach(function(e, i){
              createEdge(thisGraph,getNode(thisGraph,e.source).id,getNode(thisGraph,e.target).id,e.title);
            });           
            thisGraph.idct++;
            thisGraph.updateGraph();
            generateSentencesList(thisGraph);
          }catch(err){
            window.alert("Error parsing uploaded file\nerror message: " + err.message);
            return;
          }        
        };
        filereader.readAsText(uploadFile);

      } else {
        alert("Your browser won't let you save this graph -- try upgrading your browser to IE 10+ or Chrome or Firefox.");
      }
      
  });

  // handle delete graph
  d3.select("#delete-graph").on("click", function(){
      thisGraph.deleteGraph(false);
  });


};
// -------------------------------- /Listeners --------------------------------

// -------------------------------- Auxiliary functions --------------------------------


  // When the item with value "word" of the related list is clicked
  // this function calls the createTopicNode and sets of the 
  // parameters needed for correct showing the elements of the screen
  function relatedListClick(word,thisGraph){
   var id = (thisGraph.createTopicNode.call(thisGraph,word,false,true));	
   setNewEdge("-1");
   showTextareaRelationships();
   generateList(thisGraph,word);	
   cleanTextareaRelatedTopic();
   showTextareaRelationships();	
   event.preventDefault()
   $("#textareaEdge").focus();
 }
 
 function generateSentencesList(thisGraph){
 	d3.select("#sentences_list_ul").remove();
   d3.select("#sentences_list").append("ul").attr("id","sentences_list_ul");

   var str = "";
   thisGraph.edges.forEach(function(val, i){
    var ids = val.id.split("-");
    var sou=getNode(thisGraph,ids[0]);
    var tar=getNode(thisGraph,ids[1]);
    var Source = sou.title[0].toUpperCase() + sou.title.substring(1);

    str = Source + " " + val.title.toLowerCase() + " " + tar.title.toLowerCase() + "." ;
    d3.select("#sentences_list_ul")
    .append("li")
    .attr("class","li_related_list")
    .text(str);
  });	


 }

  // Generate the list with related words of "word"
  function generateList(thisGraph,word){

	//get the array with related words
	var related_words = getRelatedWords(word);
	
	// Screen settings
	clearRelatedTopicList();
	d3.select("#related_list").append("ul").attr("id","related_list_ul");
	
	// Adding elements to the list
	for (var i = 0;i<related_words.length;i++){
    d3.select("#related_list_ul")
    .append("li")
    .attr("id","li_"+i)
    .attr("class","li_related_list")
    .text(related_words[i])
    .on("click", function(){
     relatedListClick(this.innerText,thisGraph)
   });}
  }
  
  // Clear the textarea of the search
  function cleanTextareaRelatedTopic(){
   document.getElementById("textareaTopic").value="";
 }

  // function that will have the ajax call to get the list
  function getRelatedWords(topic){
   var result;
   switch(topic) {
    case "music":
    result = ["Adagio","Band","Beethoven","Blues","Dance","Disco","Harmony","Guitar","Justin Bieber"]; 
    break;
    case "Adagio":
    result = ["word1","word2","word3","word4","word5","word6","word9","word8","word7"];
    break;
    case "word1":
    result = ["word4","word5","word6","word9","word8","word7"];
    break;
    case "word2":
    result = ["word10","word11","word12","word9","word8","word7","word1","word2","word3","word4","word5"];
    break;
    case "word3":
    result = ["word10","word11","word12","word1","word2","word3","word4","word5"];
    break;
    default:
    result = ["word1","word2","word3","word4","word5","word6","word9","word8","word7"]; 
  }
  return result;
}

  function deleteSelectedEdge(thisGraph,selectedEdge){
    var test = "#end-arrow-"+selectedEdge.id;
    d3.select("svg defs").select(test).remove();
    thisGraph.edges.splice(thisGraph.edges.indexOf(selectedEdge), 1);
    hideTextareaRelationships();  
  }

  function deleteSelectedNode(thisGraph,idSource){
    var idSource=getSourceNodeId();
    var node_to_delete = getNode(thisGraph,idSource);
    thisGraph.nodes.splice(thisGraph.nodes.indexOf(node_to_delete), 1);
    thisGraph.spliceLinksForNode(node_to_delete);
    setSourceNode("");
    generateSentencesList(thisGraph);              
  }
// -------------------------------- /Auxiliary functions --------------------------------

/* PROTOTYPE FUNCTIONS */
GraphCreator.prototype.setIdCt = function(idct){
  this.idct = idct;
};

GraphCreator.prototype.consts =  {
  selectedClass: "selected",
  connectClass: "connect-node",
  circleGClass: "conceptg",
  graphClass: "graph",
  activeEditId: "active-editing",
  BACKSPACE_KEY: 8,
  DELETE_KEY: 46,
  DELETE_KEY_MAC: 51,
  ENTER_KEY: 13,
  ADD: 38,
  SUBTRACT: 40,
  ESCAPE_KEY : 27,
  delta_radius: 5,
  nodeRadius: 50,
  x_origin: 300,
  y_origin: 100,
  textInCircleSize: 20,
  textInPathSize: 20
};

GraphCreator.prototype.dragmove = function(d) {
  var thisGraph = this;
  if (thisGraph.state.shiftNodeDrag){
    thisGraph.dragLine.attr('d', 'M' + d.x + ',' + d.y + 'L' + d3.mouse(thisGraph.svgG.node())[0] + ',' + d3.mouse(this.svgG.node())[1]);
  } else{
    d.x += d3.event.dx;
    d.y +=  d3.event.dy;
    if (thisGraph.nodes.length>1){
     checkCollisions(thisGraph,getNode(thisGraph,getSourceNodeId()));
   }
   thisGraph.updateGraph();
 }
};

GraphCreator.prototype.deleteGraph = function(skipPrompt){
  var thisGraph = this,
  doDelete = true;
  if (!skipPrompt){
    doDelete = window.confirm("Press OK to delete this graph");
  }
  if(doDelete){
    thisGraph.nodes = [];
    thisGraph.edges = [];
	  // screen settings
	  setSourceNode("");
	  setTargetNode("");
	  clearRelatedTopicList();
    thisGraph.updateGraph();
    cleanTextareaRelatedTopic();
	generateSentencesList(thisGraph);
	$("#textareaSentencesInput").val("");
  }
};

/* select all text in element: taken from http://stackoverflow.com/questions/6139107/programatically-select-text-in-a-contenteditable-html-element */
GraphCreator.prototype.selectElementContents = function(el) {
  var range = document.createRange();
  range.selectNodeContents(el);
  var sel = window.getSelection();
  sel.removeAllRanges();
  sel.addRange(range);
};

/* insert svg line breaks: taken from http://stackoverflow.com/questions/13241475/how-do-i-include-newlines-in-labels-in-d3-charts */
GraphCreator.prototype.insertTitleLinebreaks = function (gEl, title) {
  var words = title.split(/\s+/g),
  nwords = words.length;
  var el = gEl.append("text")
  .attr("text-anchor","middle")
  .attr("dy", "-" + (nwords-1)*7.5);
  el.text(title);
 };

  // remove edges associated with a node
  GraphCreator.prototype.spliceLinksForNode = function(node) {
    var thisGraph = this,
    toSplice = thisGraph.edges.filter(function(l) {
      return (l.source === node || l.target === node);
    });
    toSplice.map(function(l) {
      thisGraph.edges.splice(thisGraph.edges.indexOf(l), 1);
    });
  };

  GraphCreator.prototype.replaceSelectEdge = function(d3Path, edgeData){
    var thisGraph = this;
    d3Path.classed(thisGraph.consts.selectedClass, true);
    if (thisGraph.state.selectedEdge){
      thisGraph.removeSelectFromEdge();
    }
    thisGraph.state.selectedEdge = edgeData;	
  };

  GraphCreator.prototype.replaceSelectNode = function(d3Node, nodeData){
    var thisGraph = this;
    d3Node.classed(this.consts.selectedClass, true);
    if (thisGraph.state.selectedNode){
      thisGraph.removeSelectFromNode();
    }
    thisGraph.state.selectedNode = nodeData;
  };

  GraphCreator.prototype.removeSelectFromNode = function(){
    var thisGraph = this;
    thisGraph.circles.filter(function(cd){
      return cd.id === thisGraph.state.selectedNode.id;
    }).classed(thisGraph.consts.selectedClass, false);
    thisGraph.state.selectedNode = null;
  };

  GraphCreator.prototype.removeSelectFromEdge = function(){
    var thisGraph = this;
    thisGraph.paths.filter(function(cd){
      return cd === thisGraph.state.selectedEdge;
    }).classed(thisGraph.consts.selectedClass, false);
    thisGraph.state.selectedEdge = null;

  };
  
  GraphCreator.prototype.pathDblclick = function(d3path, d){
    var thisGraph = this,
    state = thisGraph.state;
    d3.event.stopPropagation();
    state.mouseDownLink = d;
    
  	// set the source and target node from the information of the edge
  	var ids=d.id.split("-");
  	setSourceNode(ids[0]);
  	setTargetNode(ids[1]);
  	setNewEdge(d.id);	
  	showTextareaRelationships();  	
    var prevEdge = state.selectedEdge;
    if (!prevEdge || prevEdge !== d){
      thisGraph.replaceSelectEdge(d3path, d);
    } else{
      thisGraph.removeSelectFromEdge();
    }
    thisGraph.updateGraph();
    event.preventDefault()
    $("#textareaEdge").focus();
};

GraphCreator.prototype.pathMouseDown = function(d3path, d){
  var thisGraph = this,
  state = thisGraph.state;
  d3.event.stopPropagation();
  state.mouseDownLink = d;
  hideTextareaRelationships()
  if (state.selectedNode){
    thisGraph.removeSelectFromNode();
  }	
  // set the source and target node from the information fo the edge
	var ids=d.id.split("-");
	setSourceNode(ids[0]);
	setTargetNode(ids[1]);
	setNewEdge(d.id);	
	
	var prevEdge = state.selectedEdge;
  if (!prevEdge || prevEdge !== d){
    thisGraph.replaceSelectEdge(d3path, d);
  } else{
    thisGraph.removeSelectFromEdge();
  }
  thisGraph.updateGraph();  
  event.preventDefault();	
};

  // mousedown on node
  GraphCreator.prototype.circleMouseDown = function(d3node, d){
    var thisGraph = this,
    state = thisGraph.state;
    d3.event.stopPropagation();
    state.mouseDownNode = d;
	// color logic settings
	setSourceNode(d.id);
	setTargetNode("");
	
  thisGraph.removeSelectFromEdge();

  if (d3.event.shiftKey){
    state.shiftNodeDrag = d3.event.shiftKey;
      // reposition dragged directed edge
      thisGraph.dragLine.classed('hidden', false)
      .attr('d', 'M' + d.x + ',' + d.y + 'L' + d.x + ',' + d.y);
      return;
    }else{		
		// Create the list of related topics
		generateList(thisGraph,d.title);
		thisGraph.updateGraph();
	}
};

/* place editable text on node in place of svg text */
GraphCreator.prototype.changeTextOfNode = function(d3node, d){
  var thisGraph= this,
  consts = thisGraph.consts,
  htmlEl = d3node.node();
  d3node.selectAll("text").remove();
  var test = d3node.selectAll("circle");
  var nodeBCR = htmlEl.getBoundingClientRect(),
  curScale = nodeBCR.width/consts.nodeRadius,
  placePad  =  5*curScale,
  useHW = curScale > 1 ? nodeBCR.width*0.71 : consts.nodeRadius*1.42;
    // replace with editableconent text
    var t1 = d3.transform(d3node.attr("transform")),
    Tx = t1.translate[0],
    Ty = t1.translate[1];
    var t2 = d3.transform(d3.select(".graph").attr("transform"));
    var t3 = d3.transform(d3.select(".container_slider").attr("transform"));
    Tx = (Tx*t2.scale[0] + t2.translate[0])*t3.scale[0];
    Ty = (Ty*t2.scale[0] + t2.translate[1])*t3.scale[0];
    var d3txt = thisGraph.svg.selectAll("foreignObject")
    .data([d])
    .enter()
    .append("foreignObject")
    .attr("x", Tx-20 )
    .attr("y", Ty-20 )
    .attr("height", 2*useHW)
    .attr("width", useHW)
    .append("xhtml:p")
    .attr("id", consts.activeEditId)
    .attr("contentEditable", "true")
    .text(d.title)
    .on("mousedown", function(d){
      d3.event.stopPropagation();   
    })
    .on("keydown", function(d){
      d3.event.stopPropagation();
      if (d3.event.keyCode == consts.ENTER_KEY && !d3.event.shiftKey){                   
       this.blur();
     }
     if (d3.event.keyCode == consts.ESCAPE_KEY){

      d3.select(this.parentElement).remove();
     }
   })
    .on("blur", function(d){
      if (this.textContent!=""){
        d.title = this.textContent;        
      }
      else{
        d.title = "Topic";
      }
      thisGraph.insertTitleLinebreaks(d3node, d.title);
        generateList(thisGraph,d.title);
        generateSentencesList(thisGraph);
        thisGraph.updateGraph();
        d3.select(this.parentElement).remove();
    });
    return d3txt;
  };

  // mouseup on nodes
  GraphCreator.prototype.circleMouseUp = function(d3node, d){
    var thisGraph = this,
    state = thisGraph.state,
    consts = thisGraph.consts;
    // reset the states
    state.shiftNodeDrag = false;
    d3node.classed(consts.connectClass, false);

    var mouseDownNode = state.mouseDownNode;

    if (!mouseDownNode) return;

    thisGraph.dragLine.classed("hidden", true);

    if (mouseDownNode !== d){
      // we're in a different node: create new edge for mousedown edge and add to graph
      var newEdge = {source: mouseDownNode, target: d, title: "", id:mouseDownNode.id+"-"+d.id	};
      var defs = d3.select('svg defs');
      defs.append('svg:marker')
      .attr('id', 'end-arrow-' + newEdge.id)
      .attr('viewBox', '0 -5 10 10')
      .attr('refX', "32")
      .attr('markerWidth', 3.5)
      .attr('markerHeight', 3.5)
      .attr('orient', 'auto')
      .append('svg:path')
      .attr('d', 'M0,-5L10,0L0,5');
      var filtRes = thisGraph.paths.filter(function(d){
       if (d.source === newEdge.target && d.target === newEdge.source){
        thisGraph.edges.splice(thisGraph.edges.indexOf(d), 1);
      }
      return d.source === newEdge.source && d.target === newEdge.target;
      });
      setNewEdge(newEdge.id);
      setTargetNode(d.id);
      if (!filtRes[0].length){
       thisGraph.edges.push(newEdge);
       thisGraph.updateGraph();
     }
     showTextareaRelationships();
     event.preventDefault()
     $("#textareaEdge").focus();
   }
   else{
    hideTextareaRelationships();
		// we're in the same node
		if (state.justDragged) {
			// dragged, not clicked
			state.justDragged = false;
		}
		else
		{
			// clicked, not dragged
			if (d3.event.shiftKey){
			// shift-clicked node: edit text content
			var d3txt = thisGraph.changeTextOfNode(d3node, d);
			var txtNode = d3txt.node();
			thisGraph.selectElementContents(txtNode);
			txtNode.focus();
       }
       else
       {
        if (state.selectedEdge){
         thisGraph.removeSelectFromEdge();
       }
       var prevNode = state.selectedNode;
       if (!prevNode || prevNode.id !== d.id){
         thisGraph.replaceSelectNode(d3node, d);
       }
       else
       {
         thisGraph.removeSelectFromNode();
       }
     }
    }
    }
    state.mouseDownNode = null;
    return;

  }; // end of circles mouseup

  // mousedown on main svg
  GraphCreator.prototype.svgMouseDown = function(){
    this.state.graphMouseDown = true;
  };
  
  // double click on main svg
  GraphCreator.prototype.svgDblclick = function(d3node, d){
    var thisGraph = this,state = thisGraph.state;        
      // clicked not dragged from svg

      var mouseDownNode = state.mouseDownNode;
      if (mouseDownNode !== d){
      var type = getNodeAttributeInterface();
      switch (type){
        case "1" :
        var xycoords = d3.mouse(thisGraph.svgG.node()),
       d = {id: thisGraph.idct++, title: consts.defaultTitle, x: xycoords[0], y: xycoords[1], radius:50};
       setSourceNode(d.id);
       thisGraph.nodes.push(d);
       thisGraph.updateGraph();
       hideTextareaRelationships();
       clearRelatedTopicList();
      // make title of text immediently editable
      var d3txt = thisGraph.changeTextOfNode(thisGraph.circles.filter(function(dval){
        return dval.id === d.id;
      }), d),
      txtNode = d3txt.node();
      thisGraph.selectElementContents(txtNode);
      txtNode.focus();
      break;
      case "2" : 
       showNodeAttributesDialog();
      break;

      }
            
    }
    else{
      var d3txt = thisGraph.changeTextOfNode(d3node, d);
      var txtNode = d3txt.node();
      thisGraph.selectElementContents(txtNode);
      txtNode.focus();
    }


  };  
  
  // Places editable text on double click - TODO: look way of handle this in svgDblclick to have only one method
  GraphCreator.prototype.svgDblclick2 = function(d3node, d){
     var type = getNodeAttributeInterface();
      switch (type){
          case "1":     var thisGraph = this,state = thisGraph.state;       
               d3.event.stopPropagation();
               state.mouseDownNode = d;
               var d3txt = thisGraph.changeTextOfNode(d3node, d);
               var txtNode = d3txt.node();
               thisGraph.selectElementContents(txtNode);
               txtNode.focus();
          break;
          case "2" : 
                showNodeAttributesDialog();
          break;

      }
        

    };  

  // mouseup on main svg
  GraphCreator.prototype.svgMouseUp = function(){
    var thisGraph = this,
    state = thisGraph.state;
    if (state.justScaleTransGraph) {
      // dragged not clicked
      state.justScaleTransGraph = false;
      hideTextareaRelationships();
    } else if (state.graphMouseDown && d3.event.shiftKey){
      // clicked not dragged from svg
      var xycoords = d3.mouse(thisGraph.svgG.node()),
      d = {id: thisGraph.idct++, title: consts.defaultTitle, x: xycoords[0], y: xycoords[1], radius:50};
      setSourceNode(d.id);
      thisGraph.nodes.push(d);
      thisGraph.updateGraph();
      // make title of text immediently editable
      var d3txt = thisGraph.changeTextOfNode(thisGraph.circles.filter(function(dval){
        return dval.id === d.id;
      }), d),
      txtNode = d3txt.node();
      thisGraph.selectElementContents(txtNode);
      txtNode.focus();
      hideTextareaRelationships();
    } else if (state.shiftNodeDrag){
      // dragged from node
      state.shiftNodeDrag = false;
      thisGraph.dragLine.classed("hidden", true);

    }

    state.graphMouseDown = false;
  };  
  
  // Creates a node with the topic = "new"
  GraphCreator.prototype.createTopicNode = function(text,source,target){
    var thisGraph = this,
    state = thisGraph.state;

    // clicked not dragged from svg
    var id_node_source = getSourceNodeId(),
     x_coord = thisGraph.consts.x_origin,
     y_coord = thisGraph.consts.y_origin;
	  
    // If source node selected
	  if (id_node_source!=""){
      var node = getNode(thisGraph, id_node_source);
      x_coord = node.x;
      y_coord = node.y + 200;
    }	  

	  // Id for new node
	  var id = thisGraph.idct++;
	  
	  var xycoords = d3.mouse(thisGraph.svgG.node()),
    d = {id: id, title: text, x: x_coord, y: y_coord, radius:thisGraph.consts.nodeRadius} ;
    
    if (source){setSourceNode(d.id);}
    
    if (target){setTargetNode(d.id);}
    
    thisGraph.nodes.push(d);

    var newNode = getNode(thisGraph,id);

	  // check if is in users view
	  if (thisGraph!==undefined){
      if (thisGraph.nodes.length>1){				
			// If collisions with another node, moves the new one
			checkCollisionsNewNode(thisGraph,getNode(thisGraph,id));				
		}
 }	  
 thisGraph.updateGraph();	
 return d.id;    
};

function zoomWithSliderLibrary(scale) {
  var svg = d3.select("body").select("svg");
  var container = d3.select(".container_slider");
  var h = svg.attr("height"), w = svg.attr("width");		
  container.attr("transform",
    "scale(" + scale + ") "               
    );
}
  // detect collision:
  // C1 with center (x1,y1) and radius r1;
  // C2 with center (x2,y2) and radius r2.
  //(x2-x1)^2 + (y1-y2)^2 <= (r1+r2)^2
  // meaning the distance between the center points is less than the sum of the rads.
  // sqrt used cause overflow
  function collision(nodeA, nodeB){
   var r=50;
   var x=Math.abs(nodeB.x-nodeA.x);
   var y= Math.abs(nodeB.y-nodeA.y);
   var r2= Math.abs(nodeA.radius + nodeB.radius);
   if( Math.sqrt(x * x+ y * y) <= r2 ){
    return true;
   }
   return false;
}

  // Check if the node is in the visual field of the user
  function checkNodeInUserView(thisGraph,actualNode){
   var svg_width = d3.select("body").select("svg").style("width");
   var svg_height = d3.select("body").select("svg").style("height");
   var x_length = parseInt(svg_width.replace("px",""));
   var y_length = parseInt(svg_height.replace("px",""));
   var x_start = 0;
   var y_start = 0;

   var t1 = d3.transform(d3.select('.container_slider').attr("transform")),
   S2=t1.scale[0];

   var t2 = d3.transform(d3.select('.graph').attr("transform")),
   Tx = t2.translate[0],
   Ty = t2.translate[1],
   S1=t1.scale[0];
	
	var result = true;
	var position = ((((S1*actualNode.y)+Ty)*S2)+50);
	if (y_length< position){

		return false;
	}
	return result;
}

  // Check collisions for new node
  // if collision, moves the node
  // function createTopicNode
  function checkCollisionsNewNode(thisGraph,actualNode){
   var restart = true;
   while (restart){
    restart = false;
    for (var i = 0; i < thisGraph.nodes.length; i++){
     if (thisGraph.nodes[i].id != actualNode.id){						
       if (collision(actualNode,thisGraph.nodes[i])){
        restart = true;
        actualNode.x++;						
      }				
    }
  }
}
}

  // Check collisions
  // if collision, move the node that is crashing with actualNode
  // function for moving nodes around the graph
  function checkCollisions(thisGraph,actualNode){
   var restart = true;	
	// if there is collision, we have to restart the procedure to see if
	// the movement of the node didnt make another collision
	while (restart){
   restart = false;
   for (var i = 0; i < thisGraph.nodes.length; i++){
     if (thisGraph.nodes[i].id != actualNode.id){

       if (collision(actualNode,thisGraph.nodes[i])){
        restart = true;
        if ((thisGraph.nodes[i].y == actualNode.y )&&(thisGraph.nodes[i].x == actualNode.x)){
							//exactly same place, new node
							thisGraph.nodes[i].x+= 2*50+10;						
						}
						else{
							if ((thisGraph.nodes[i].y - actualNode.y )>0){
								thisGraph.nodes[i].y++;
							}
							else{
								thisGraph.nodes[i].y--;
							}
							if ((thisGraph.nodes[i].x - actualNode.x )>0){
								thisGraph.nodes[i].x++;
							}
							else{
								thisGraph.nodes[i].x--;
							}
						}
						checkCollisions(thisGraph,thisGraph.nodes[i]);
					}

       }
     }
   }
 }

// Creates a new edge
 function createEdge(thisGraph,source_id,target_id,edge_title){
   var newEdge = { source: getNode(thisGraph,source_id),
     target: getNode(thisGraph,target_id),
     title: edge_title,
     id: source_id + "-" + target_id
    };
   var defs = d3.select('svg defs');
   defs.append('svg:marker')
   .attr('id', 'end-arrow-' + newEdge.id)
   .attr('viewBox', '0 -5 10 10')
   .attr('refX', "32")
   .attr('markerWidth', 3.5)
   .attr('markerHeight', 3.5)
   .attr('orient', 'auto')
   .append('svg:path')
   .attr('d', 'M0,-5L10,0L0,5');							   
   thisGraph.edges.push(newEdge);						
}	

// -------------------------------- Key events handlers	--------------------------------
   // keydown on textareaEdge svg
   GraphCreator.prototype.textareaEdgeKeyDown = function(){
    var thisGraph = this,
    state = thisGraph.state,
    consts = thisGraph.consts;
    switch(d3.event.keyCode) {
      case consts.BACKSPACE_KEY:
      if ($("#textareaEdge").is(':focus')){
        var e = jQuery.Event("keydown", { keyCode: 20 });
        $("#textareaEdge").trigger( e );				
      }
      break;
      case consts.ENTER_KEY: 
      d3.event.preventDefault();			
      var edge = getNewEdge()
      if (edge=="-1")
      {	
        if(getTargetNodeId()!=""){									
          createEdge(thisGraph,getSourceNodeId(),getTargetNodeId(),$("#textareaEdge").val());
          setSourceNode(getTargetNodeId());				
         }
       }else{
        var ids = edge.split("-");			
        if ((ids[0]==getSourceNodeId()) && (ids[1]==getTargetNodeId())){

         var edge= thisGraph.edges.filter(function(n){return (n.source.id == getSourceNodeId() && n.target.id == getTargetNodeId())})[0];
         edge.title = document.getElementById("textareaEdge").value;
         setSourceNode(getTargetNodeId());										
       }
       else{
         setSourceNode("");
       }
       setNewEdge("-1");
       clearRelatedTopicList();
     }
     setTargetNode("");
     generateSentencesList(thisGraph);
     thisGraph.updateGraph();

     cleanTextareaEdge();
     hideTextareaRelationships();
     break;
   }
 };

   // keydown on textareaSentencesInput svg
   GraphCreator.prototype.textareaSentencesInputDown = function() {
    var thisGraph = this,
    state = thisGraph.state,
    consts = thisGraph.consts;
    switch(d3.event.keyCode) {
      case consts.BACKSPACE_KEY:
      if ($("#textareaSentencesInput").is(':focus')){
        var e = jQuery.Event("keydown", { keyCode: 20 });
        $("#textareaSentencesInput").trigger( e );        
      }
      break;
      case consts.ENTER_KEY: 
      d3.event.preventDefault(); 
      if (document.getElementById("textareaSentencesInput").value!=""){     
      var sentence = (document.getElementById("textareaSentencesInput").value).split(" ");

      var source = thisGraph.nodes.filter(function(n){return n.title == sentence[0];})[0];
      var target = thisGraph.nodes.filter(function(n){return n.title == sentence[2];})[0];
      var edge_title = sentence[1];     


      if(source!==undefined){ // Exist node with topic on the input (source)
        if(target!==undefined){ // Exist node with topic on the input (target)
          //1 1         
        }else{  
          //1 0         
          var id_target = thisGraph.idct++;       
          
          target = {id: id_target, title: sentence[2], x: source.x, y: source.y + 200, radius:50} ;
          
          checkCollisions(thisGraph,target);
          
          thisGraph.nodes.push(target);
        }
      }else{        
        var id_source = thisGraph.idct++;       
        
        source = {id: id_source, title: sentence[0], x: 100, y: 100, radius:50} ;
        
        if (thisGraph.nodes.length>0){
          checkCollisions(thisGraph,source);
        }
        thisGraph.nodes.push(source);
        
        if(target!==undefined){ // Exist node with topic on the input (target)
          //0 1
          
          source.x = target.x;
          
          source.y = target.y - 200;

          checkCollisions(thisGraph,source);
          
        }else{
          //0 0
          
          var id = thisGraph.idct++;        
          
          target = {id: id, title: sentence[2], x: source.x, y: source.y + 200, radius:50} ;
          
          checkCollisions(thisGraph,target);

          thisGraph.nodes.push(target);
        }     
      }     
      createEdge(thisGraph,source.id,target.id,edge_title);       
      thisGraph.updateGraph();
      $("#textareaSentencesInput").val("");
      generateSentencesList(thisGraph);
      }
     break;}
  };

 // keydown on textareatopic svg
  GraphCreator.prototype.textareaTopicKeyDown = function() {
    var thisGraph = this,
    state = thisGraph.state,
    consts = thisGraph.consts;
    switch(d3.event.keyCode) {
      case consts.BACKSPACE_KEY:
      // Code for giving the backspace the common functionality when focused in the textarea
        if ($("#textareaTopic").is(':focus')){
          var e = jQuery.Event("keydown", { keyCode: 20 });
          $("#textareaTopic").trigger( e );       
        }
      break;

      case consts.ENTER_KEY: 
        //Generates the new node and the related list
        d3.event.preventDefault();     
        if (document.getElementById("textareaTopic").value!=""){ 
        var id = (thisGraph.createTopicNode.call(thisGraph,document.getElementById("textareaTopic").value,true,false));
        generateList(thisGraph,document.getElementById("textareaTopic").value);   
        d3.select("#related_list_title").style("display","block");
        }    
      break;}
  
};


  // keydown on main svg
  GraphCreator.prototype.svgKeyDown = function() {
    
    var thisGraph = this,
    state = thisGraph.state,
    consts = thisGraph.consts;
  
    // make sure repeated key presses don't register for each keydown
    if(state.lastKeyDown !== -1) return;

    state.lastKeyDown = d3.event.keyCode;
    var selectedNode = state.selectedNode,
    selectedEdge = state.selectedEdge;
    switch(d3.event.keyCode) {
      case consts.BACKSPACE_KEY:
      // Check if the backspace key was not fired from a textarea
      if(!$("#textareaEdge").is(':focus') && !$("#textareaTopic").is(':focus') && !$("#textareaSentencesInput").is(':focus')){
       d3.event.preventDefault();
       if (selectedEdge){
          // If there is an edge selected => delete
          deleteSelectedEdge(thisGraph,selectedEdge);
          state.selectedEdge = null;
        } 
        else{
          var idSource=getSourceNodeId();
          if(idSource!=""){
            // If there is a node selected => delete
              deleteSelectedNode(thisGraph,idSource);
              state.selectedNode = null;   
            }            
        }
        thisGraph.updateGraph();
        generateSentencesList(thisGraph);
      }
    break;
    case consts.ENTER_KEY: 

        d3.event.preventDefault();      
    break;
    case consts.DELETE_KEY:
        d3.event.preventDefault();
        if(!$("#textareaEdge").is(':focus') && !$("#textareaTopic").is(':focus') && !$("#textareaSentencesInput").is(':focus')){
          if (selectedEdge){
            // If there is an edge selected => delete
            deleteSelectedEdge(thisGraph,selectedEdge);
            state.selectedEdge = null;
          } 
          else{
            var idSource=getSourceNodeId();
            if(idSource!=""){
              // If there is a node selected => delete
                deleteSelectedNode(thisGraph,idSource);
                state.selectedNode = null;   
              }            
          }
          thisGraph.updateGraph();
          generateSentencesList(thisGraph);
      }
      break;  
    case consts.ADD:
      if (!$("#textareaEdge").is(':focus')){
        var idSource=getSourceNodeId();
        if (idSource!=""){         
           var node_selected = getNode(thisGraph,idSource);
           node_selected.radius+=thisGraph.consts.delta_radius;
           thisGraph.updateGraph();
        }
      }
    break;
   case consts.SUBTRACT:
     if (!$("#textareaEdge").is(':focus')){
       var idSource=getSourceNodeId();
       if (idSource!=""){     
         var node_selected = getNode(thisGraph,idSource);
         node_selected.radius-=thisGraph.consts.delta_radius;
         thisGraph.updateGraph();
       }
      }
   break;
 }
};

GraphCreator.prototype.svgKeyUp = function() {
  this.state.lastKeyDown = -1;
};
// -------------------------------- /Key events handlers --------------------------------

function getClass(d){  
	if (d.id == parseInt(getSourceNodeId()) ) { return "conceptg selected"; }
  if (d.id == parseInt(getTargetNodeId()) ) { return "conceptg target"; }		
 return "conceptg";
}

function updatePaths(thisGraph){
  var consts = thisGraph.consts,
    state = thisGraph.state;
var paths = thisGraph.paths;
  paths.style('marker-end',  function(d){
    return 'url(#end-arrow-'+d.id+')'})
  .classed(consts.selectedClass, function(d){
    return d === state.selectedEdge;
  })
  .attr("d", function(d){
    return "M" + d.source.x + "," + d.source.y + "L" + d.target.x + "," + d.target.y;
  });
}

function addNewPaths(thisGraph){
    var consts = thisGraph.consts,
    state = thisGraph.state,
    paths = thisGraph.paths;
  paths.enter()
    .append("path")
    .style('marker-end',function(d){
      return 'url(#end-arrow-'+d.id+')'})
    .classed("link", true)
    .attr("id",function(d){return d.id;})
    .attr("d", function(d){
      return "M" + d.source.x + "," + d.source.y + "L" + d.target.x + "," + d.target.y;
    })
    .on("mousedown", function(d){
      thisGraph.pathMouseDown.call(thisGraph, d3.select(this), d);
    })
    .on("mouseup", function(d){
      state.mouseDownLink = null;
    })
    .on("dblclick", function(d){
      thisGraph.pathDblclick.call(thisGraph, d3.select(this), d);
    });

}

// Set the position of the arrohead
// Depends on the radius of the node
function setArrowheads(thisGraph){
  var paths = thisGraph.paths;
  paths.each(function(d){       
      var r = 32 + (d.target.radius - 50)/2.05;
      d3.select("#end-arrow-"+d.id).attr('refX', r);
    });

}

function addVerbsToPath(thisGraph){
  var paths = thisGraph.paths;
  d3.selectAll("svg .verbTexts").remove();
  paths.each(function(d){   
       var startX = parseInt(d.source.x),startY = parseInt(d.source.y),
       endX = parseInt(d.target.x),endY = parseInt(d.target.y),
       angle = Math.atan2(endY - startY, endX - startX);
       //startX = startX + Math.cos(angle) * d.source.radius;
       //endX = endX + Math.sin(angle) * d.source.radius;
       //startY = startY + Math.cos(angle) * d.target.radius;
       //endY = endY + Math.sin(angle) * d.target.radius;
       var centrePointX = ((startX + endX) / 2),centrePointY = ((startY + endY) / 2),
       dist = 15 + (d.title.length*2),
       dx = (Math.sin(angle) * dist + centrePointX),dy = (-Math.cos(angle) * dist + centrePointY),
       distance = Math.sqrt( ((endX - startX)*(endX - startX)) + ((endY - startY)*(endY - startY)) ),// Distance between the nodes
       verb = "";
       if (distance - d.source.radius - d.target.radius > 35){
        verb = d.title;
       }
       var el = d3.select(".graph")
       .append("text")
       .classed("verbTexts",true)
       .attr("id",d.source.id+d.target.id)
       .attr("text-anchor","middle")    
       .attr("transform","translate("+ dx + "," + dy +")")
       .attr("font-size", thisGraph.consts.textInPathSize + "px");
        var tspan = el.append('tspan').text(verb);
        tspan.attr('x', 0).attr('dy', '0');
    });

}

function updateCircles(thisGraph){
    thisGraph.circles = thisGraph.circles.data(thisGraph.nodes, function(d){ return d.id;});
    thisGraph.circles
    .attr("id", function(d){return "node_"+d.id;} )
    .attr("class", function(d){return getClass(d);})
    .attr("transform", function(d){return "translate(" + d.x + "," + d.y + ")";});

    // Update circle radius and text size
    thisGraph.circles.each(function(d){
      d3.select("#node_"+d.id + " circle").attr("r",d.radius);
      var text_size = 0;
      if (d.radius>15 + d.title.length*2){
        text_size = thisGraph.consts.textInCircleSize + ((d.radius-50)/5);
      }
      
      d3.select("#node_"+d.id + " text").attr("font-size",  text_size + "px");
    });

}

function addNewCircles(thisGraph){
  var newGs= thisGraph.circles.enter()
    .append("g");    
    newGs.attr("class", function(d){ return getClass(d);})
    .attr("id", function(d){return "node_"+d.id;} )
    .attr("transform", function(d){return "translate(" + d.x + "," + d.y + ")";})
    .on("mouseover", function(d){
      if (thisGraph.state.shiftNodeDrag){
        d3.select(this).classed(thisGraph.consts.connectClass, true);
      }
    })
    .on("mouseout", function(d){
      d3.select(this).classed(thisGraph.consts.connectClass, false);
    })
    .on("mousedown", function(d){
      thisGraph.circleMouseDown.call(thisGraph, d3.select(this), d);
    })
    .on("mouseup", function(d){
      thisGraph.circleMouseUp.call(thisGraph, d3.select(this), d);
    })
    .on("dblclick", function(d){
     thisGraph.svgDblclick2.call(thisGraph, d3.select(this), d);})
    .call(thisGraph.drag);
    newGs.append("circle").attr("r",function(d){ return d.radius});

    newGs.each(function(d){
      thisGraph.insertTitleLinebreaks(d3.select(this), d.title);
    });

}

  // call to propagate changes to graph
  GraphCreator.prototype.updateGraph = function(){

    var thisGraph = this,
    consts = thisGraph.consts,
    state = thisGraph.state;

    thisGraph.paths = thisGraph.paths.data(thisGraph.edges, function(d){
      return String(d.source.id) + "+" + String(d.target.id);
    });   

	  // update existing paths
    updatePaths(thisGraph);

    // add new paths
    addNewPaths(thisGraph);

    // Set the position of the Arrowheads
    setArrowheads(thisGraph);

	  // Add verbs to paths
	  addVerbsToPath(thisGraph);
    
    // remove old links
    thisGraph.paths.exit().remove();

    // update existing nodes
    updateCircles(thisGraph);

    // add new nodes
    addNewCircles(thisGraph);

    // remove old nodes
    thisGraph.circles.exit().remove();
  };

  GraphCreator.prototype.zoomed = function(){
    this.state.justScaleTransGraph = true;
    d3.select("." + this.consts.graphClass)
    .attr("transform", "translate(" + d3.event.translate + ") scale(" + d3.event.scale + ")");
  
};

  GraphCreator.prototype.updateWindow = function(svg){
    var docEl = document.documentElement,
    bodyEl = document.getElementsByTagName('body')[0];
    var x = window.innerWidth || docEl.clientWidth || bodyEl.clientWidth;
    var y = window.innerHeight|| docEl.clientHeight|| bodyEl.clientHeight;
    svg.attr("width", x).attr("height", 800);
  };

  /**** MAIN ****/
  var docEl = document.documentElement,
  bodyEl = document.getElementsByTagName('body')[0];

  //var width = window.innerWidth || docEl.clientWidth || bodyEl.clientWidth,
  var width = (window.innerWidth || docEl.clientWidth || bodyEl.clientWidth),
  height =  window.innerHeight|| docEl.clientHeight|| bodyEl.clientHeight;

  var xLoc = width/2 - 25,
  yLoc = 100;

  // initial node data
  var nodes = [];
  var edges = [];


  /** MAIN SVG **/
  
  var svg = d3.select(settings.appendElSpec).append("svg")
  .attr("width", width)
  .attr("height", 800)
  .attr("id","svg");

  var graph = new GraphCreator(svg, nodes, edges);

  graph.setIdCt(2);
  graph.updateGraph();

})(window.d3, window.saveAs, window.Blob);
