Topic Modeler Version 0.9 26/08/2014

Topic Modeler helps you get started with your writing. Start from an idea, get related topics, make relationships, give to your scratch the order you want in a friendly visual design

GENERAL USAGE NOTES
--------------------

- Shift+click or double click for creating nodes.

- Clicking on nodes to select it, a list of related words will appear, you can click in any of them to create another node and start the same process from there.

- Add relationships or orders to your nodes. You can give them a tag if you want.

- Export the graph as a JSON so you can continue working on it whenever you want.

- Delete graph to start over.

- Export the sentence you have created, or the whole graph in PNG.

- Up / Down arrow for resizing selected node

---------------------------------------------------------------------------

Installing
------------

Just run index.html in any browser. No internet conection needed.

============================================================================

Web site : 
e-mail: 

Copyright ....................